# Golang algorithms

### - `"gitlab.com/benjamin.grondin/golang-algorithms/sort"`

--- 

This repository contains several functions using algorithms writed in Golang.

You will meet there:
- Bubble sort
- Comb sort
- Insertion sort
- Merge sort
- Quick sort
- Selection sort
- Shell sort

--- 

## Presentation

```go
type Sort interface {
	GetName() string // Return name of algorithm
	Sort([]int) ([]int, int) // Return array sorted
}
```
All algorithms using this interface. You can get name of algorithm and use an function for sort an array of int (`[]int`) 

```go
var algo sort.Sort

// Bubble sort
algo = sort.BubbleSort{}
// Comb sort
algo = sort.CombSort{}
// Insertion sort
algo = sort.InsertionSort{}
// Merge sort
algo = sort.MergeSort{}
// Quick sort
algo = sort.QuickSort{}
// Selection sort
algo = sort.SelectionSort{}
// Shell sort
algo = sort.ShellSort{}	
```

---

## Makefile

- `make run`: Run program.
- `make build` Build program into binary.
- `make clean`: Clean repositorie by deleting the folder `./bin`.
- `make hello`: Returns general program information.
- `make all`: Run command `hello`, `run` and `build`.

--- 

## Example

```go
var algo sort.Sort

algo = sort.BubbleSort{}

array := []int{3,10,5}

algo.Sort(array)
// output: [3,5,10]

algo.GetName()
// output: Bubble sort
```