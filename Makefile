run:
	go run .
	@echo "Done."

build:
	@echo "Building.."
	go build -o bin/goalgorithms .
	@echo "Done."

clean:
	@echo "Cleaning repository.."
	rm -Rf bin
	@echo "Done."

hello:
	@echo "This repository contains several functions using algorithms writed in Golang.\nYou will meet there:\n"
	@echo "\t- Bubble-sort"
	@echo "\t- Comb-sort"
	@echo "\t- Insertion-sort"
	@echo "\t- Merge-sort"
	@echo "\t- Quick-sort"
	@echo "\t- Selection-sort"
	@echo "\t- Shell-sort"

all: hello run build