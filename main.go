package main

import (
	"fmt"
	"time"

	"gitlab.com/benjamin.grondin/golang-algorithms/sort"
)

func main() {
	fmt.Println()
	fmt.Println("@Author: Benjamin Grondin - 2022.")
	fmt.Println("@Project:  Golang algorithms.")
	fmt.Println()

	algos := []sort.Sort{
		sort.BubbleSort{},
		sort.CombSort{},
		sort.InsertionSort{},
		sort.MergeSort{},
		sort.QuickSort{},
		sort.SelectionSort{},
		sort.ShellSort{},
	}

	for _, algo := range algos {
		arr1 := []int{3, 4, 5, 2, 1, 13, 25, -5, -13, 34, 7, 14, 90, 63, 768, 321, -356, -24, 2, -45, -5, 23, 6, 7, 8, -1, -3}

		fmt.Println("- Algorithm: ", algo.GetName())
		fmt.Println("\t- Before : ", arr1)

		starttime := time.Now()
		sortedarray, count := algo.Sort(arr1)
		endtime := time.Now().Sub(starttime)

		fmt.Println("\t- Sorted : ", sortedarray)
		fmt.Println("\t- Time   : ", endtime)
		fmt.Println("\t- Count  : ", count)
		fmt.Println()
	}

	fmt.Println("Done.")
}
