package sort

type QuickSort struct{}

func (qs QuickSort) GetName() string {
	return "Quick sort"
}

func partition(array []int, low, high int, count int) (int, int) {
	i := low - 1
	pivot := array[high]

	for j := low; j <= high-1; j++ {
		count++
		if array[j] <= pivot {
			i += 1
			array[i], array[j] = array[j], array[i]
		}
	}

	array[i+1], array[high] = array[high], array[i+1]
	return i + 1, count
}

func combine(array []int, low, high int, count int) ([]int, int) {
	p := 0
	if low < high {
		p, count = partition(array, low, high, count)
		array, _ = combine(array, low, p-1, count)
		array, _ = combine(array, p+1, high, count)
	}

	return array, count
}

func (qs QuickSort) Sort(array []int) ([]int, int) {
	count := 0

	n := len(array)
	array, count = combine(array, 0, n-1, count)

	return array, count
}
