package sort

type SelectionSort struct{}

func (ss SelectionSort) GetName() string {
	return "Selection sort"
}

func (ss SelectionSort) Sort(array []int) ([]int, int) {
	count := 0
	var i = 1
	for i < len(array)-1 {
		count += 1
		var j = i + 1
		var minIndex = i

		if j < len(array) {
			if array[j] < array[minIndex] {
				minIndex = j
			}
			j++
		}

		if minIndex != i {
			var temp = array[i]
			array[i] = array[minIndex]
			array[minIndex] = temp
		}

		i++
	}

	return array, count
}
