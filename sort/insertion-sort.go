package sort

type InsertionSort struct{}

func (is InsertionSort) GetName() string {
	return "Insertion sort"
}

func (is InsertionSort) Sort(array []int) ([]int, int) {
	count := 0
	selector := 1

	for selector < len(array) {
		count += 1
		comparator := selector

		for comparator >= 1 && array[comparator] < array[comparator-1] {
			count += 1
			array[comparator], array[comparator-1] = array[comparator-1], array[comparator]
			comparator--
		}

		selector++
	}

	return array, count
}
