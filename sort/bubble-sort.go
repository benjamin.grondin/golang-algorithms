package sort

type BubbleSort struct{}

func (bs BubbleSort) GetName() string {
	return "Bubble sort"
}

func (bs BubbleSort) Sort(array []int) ([]int, int) {
	count := 0

	arraylength := len(array)
	for i := 0; i < arraylength-1; i++ {
		count += 1

		for j := 0; j < arraylength-i-1; j++ {
			count += 1

			if array[j] > array[j+1] {
				array[j], array[j+1] = array[j+1], array[j]
			}
		}
	}

	return array, count
}
