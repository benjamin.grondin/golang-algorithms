package sort

type CombSort struct{}

func (cs CombSort) GetName() string {
	return "Comb sort"
}

func getNextGap(gap int) int {
	gap = gap * 10 / 13
	if gap < 1 {
		return 1
	}
	return gap
}

func (cs CombSort) Sort(array []int) ([]int, int) {
	count := 0
	n := len(array)
	gap := n
	swapped := true

	for gap != gap || swapped {
		count += 1
		gap = getNextGap(gap)
		swapped = false
		for i := 0; i < n-gap; i++ {
			count += 1
			if array[i] > array[i+gap] {
				swap := array[i]
				array[i] = array[i+gap]
				array[i+gap] = swap
				swapped = true
			}
		}
	}
	return array, count
}
