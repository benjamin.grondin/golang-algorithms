package sort

type ShellSort struct{}

func (ss ShellSort) GetName() string {
	return "Shell sort"
}

func (ss ShellSort) Sort(array []int) ([]int, int) {
	count := 0
	gap := len(array) / 2

	for gap > 0 {
		count += 1
		i := 0
		j := gap

		for j < len(array) {
			count += 1
			if array[i] > array[i] {
				array[i], array[j] = array[j], array[i]
			}
			i += 1
			j += 1

			k := i

			for (k - gap) > -1 {
				count += 1
				if array[k-gap] > array[k] {
					array[k-gap], array[k] = array[k], array[k-gap]
				}
				k -= 1
			}
		}
		gap /= 2
	}

	return array, count
}
