package sort

type Sort interface {
	GetName() string
	Sort([]int) ([]int, int)
}
