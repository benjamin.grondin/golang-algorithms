package sort

type MergeSort struct{}

func (ms MergeSort) GetName() string {
	return "Merge sort"
}

func merge(fp []int, sp []int) ([]int, int) {
	count := 0
	var n = make([]int, len(fp)+len(sp))

	var fpIndex = 0
	var spIndex = 0

	var nIndex = 0

	for fpIndex < len(fp) && spIndex < len(sp) {
		count += 1
		if fp[fpIndex] < sp[spIndex] {
			n[nIndex] = fp[fpIndex]
			fpIndex++
		} else {
			n[nIndex] = sp[spIndex]
			spIndex++
		}

		nIndex++
	}

	for fpIndex < len(fp) {
		count += 1
		n[nIndex] = fp[fpIndex]

		fpIndex++
		nIndex++
	}

	for spIndex < len(sp) {
		count += 1
		n[nIndex] = sp[spIndex]

		spIndex++
		nIndex++
	}

	return n, count
}

func (ms MergeSort) Sort(array []int) ([]int, int) {
	if len(array) == 1 {
		return array, 0
	}

	var fp, _ = ms.Sort(array[:len(array)/2])
	var sp, _ = ms.Sort(array[len(array)/2:])

	return merge(fp, sp)
}
